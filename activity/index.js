console.log("ACTIVITY")

// Item 1
let number = 4
const getCube = number ** 3
let answer = 'The cube of ' + number + " is "
console.log(answer + getCube)

// Item 2

const address = {
	street: "123 Street.",
	city: "Cabanatuan City",
	province: "Nueva Ecija",
	country: "Philippines"
}

const {street, city, province, country} = address

console.log(`I live at ${street} ${city} ${province} ${country}.`)


// Item 3

const animal = {
	animalName: "Lolong",
	animalWeight: "1075 kgs",
	animalMeasurement: "20ft 3in"
}

const {animalName, animalWeight, animalMeasurement} = animal

console.log(`${animalName} was a saltwater crocodile. He weighed at ${animalWeight} with a measurement of ${animalMeasurement}.`)

// Item 4

const numbers = [1, 1, 1, 1, 1, 10]

numbers.forEach((number, index) => {
	console.log(index + number)
})

// Item 5

class Dog {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const myDog = new Dog("Chimon", 2, "Shih Tzu")
console.log(myDog)