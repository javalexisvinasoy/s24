// console.log("Hello!")

// ES6 Updates

// Exponent Operator: we use ** for exponents

const firstNum = Math.pow(8, 2)
console.log(firstNum)

const secondNum = 8 ** 2
console.log(secondNum)

// read as 5 raised to the power of 5
const thirdNum = 5 ** 5
console.log(thirdNum)

// Template Literals
/*
	Allows us to write strings without using the concatination operator (+)
*/

let name = "George"

// Concatination / Pre-Template Literal

// Using Single Quote ' '

let message = 'Hello ' + name + ' Welcome to Zuitt Coding Bootcamp!'
console.log("Message without Template Literal: " + message)

console.log(" ")

// Strings using Template Literal

// Uses the backticks ` `

message = `Hello ${name}. Welcome to Zuitt Coding Bootcamp!`
console.log(`Message with Template Literal: ${message}`)

let anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8**2 with the solution of ${secondNum}
`

console.log(anotherMessage)

anotherMessage = "\n " + name + ' attended a math competition. \n He won it by solving the problem 8**2 with the solution ' + secondNum + '. \n'

console.log(anotherMessage)


// Operation inside Template Literal

const interestRate = .1
const principal = 1000

console.log(`The interest on your savongs is: ${principal * interestRate}`)



// Array Destructuring

/*
	- Allows to unpacak elements in an array into distinct variables.
	- Allows us to name the array elements with variables instead of index number

	Syntax:
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Joe", "Dela", "Cruz"]
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`)


console.log(" ")
// Array Destructuring

const [firstName, middleName, lastName] = fullName

console.log(firstName)
console.log(middleName)
console.log(lastName)

console.log(`Hello, ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)


// Object Destructuring
/*
	Allows to unpack properties of objects into distinct variables. Shortends the syntax for accessng properties from objects

	Syntax:
		let/const {propertyName, propertyName, propertyName} = 	 object
*/

console.log(" ")
const person = {
	firstName2: "Jane",
	middleName2: "Dela",
	familyName2: "Cruz"
}

// Pre-object destructuring
console.log(person.firstName2)
console.log(person.middleName2)
console.log(person.familyName2)

function getFullName(firstName2, middleName2, familyName2) {
	console.log(`${firstName2} ${middleName2} ${familyName2}`)
}

getFullName(person.firstName2, person.middleName2, person.familyName2)


// Using Object Destructuring

const {middleName2, familyName2, firstName2} = person

console.log(firstName2)
console.log(middleName2)
console.log(familyName2)

function getFullName(firstName2, middleName2, familyName2) {
	console.log(`${firstName2} ${middleName2} ${familyName2}`)
}

getFullName(firstName2, middleName2, familyName2)

// MINI ACTIVITY 1
/*Item 1.)
		- Create a variable employees with a value of an array containing names of employees
		- Destructure the array and print out a message with the names of employees using Template Literals*/

console.log(" ")
console.log("MINI ACTIVITY")

const employees = ["Nick Dela Cruz", "Annie Santos", "John Geronimo"]

const [employee1, employee2, employee3] = employees

console.log(`Hello, ${employee1}, ${employee2}, ${employee3}! Welcome to the company! `)

console.log(" ")

/*Item 2.)
		- Create a variable pet with a value of an object data type with different details of your pet as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/

const myPet = {
	namePet: "whitey",
	breed: "poodle",
	color: "black"
}

const {namePet, breed, color} = myPet

console.log(`My pet's name is ${namePet}. He is a ${breed}. His color is ${color}.`)


// PRE-ARROW FUNCTION AND ARROW FUNCTION

/*
	Pre-Arrow Function

	Syntax:
		function functionName(paramA, paramB) {
			statement // console.log // return
		}
*/

console.log(" ")
function printFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName)
}

printFullName("Rupert", "B", "Ramos")


// Arrow Function
/*
	Syntax:
		let/const variableName = (paramA, paramB) => {
			statement // console.log // return
		}

*/

const printFullName2 = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`)
}

printFullName2("Jonh","O","Vinasoy")


const students = ["Yoby", "Emman", "Ronel"]

// Function with LOOP

// Pre-Arrow Function

students.forEach(function(student) {
	console.log(student + " is a student")
})

// Arrow Function
console.log(" ")
students.forEach((student) => {
	console.log(`${student} is a student`)
})


// Implicit Return Statement
console.log(" ")

// Pre-arrow
function add(x, y) {
	return x + y
}

let total = add(12, 15)
console.log(total)


// Arrow Function

const addition = (x, y) => x + y

/*
	// use return keyword when curly braces is present
/*const addition = (x,y) => {
	return x + y
}*/




let total2 = addition(12, 15)
console.log(total2)


// Default Function Argument Value

const greet = (name = "User") => {
	return `Good evening, ${name}`
}

console.log(greet())
console.log(greet("Archie"))


// Class-Based Object Blueprint
/*
	Allows creation/instantiation of objects using class as blueprint

	Syntax:
		class className {
			constructor (objectPropertyA, objectPropertyB) 
			this.objectPropertyA = objectPropertyA
			this.objectPropertyB = objectPropertyB
		}
*/
console.log(" ")

class Car {
	constructor(brand, name, year) {
		this.brand = brand
		this.name = name
		this.year = year
	}
}

const myCar = new Car()
console.log(myCar)

myCar.brand = "Ford"
myCar.name = "Ranger Raptor"
myCar.year = "2021"

console.log(myCar)


const myNewCar = new Car("Toyota", "Vios", 2019)
console.log(myNewCar)

